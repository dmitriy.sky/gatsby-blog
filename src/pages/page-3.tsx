// Gatsby supports TypeScript natively!
import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const ThirdPage = () => (
  <Layout>
    <SEO title="Third Page" />
    <h1>Page 3</h1>
  </Layout>
)

export default ThirdPage
