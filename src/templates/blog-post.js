import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'

const BlogPost = ({ data }) => {
  const post = data.markdownRemark
  return (
    <Layout>
      <h1>{post.frontmatter.title}</h1>
      <p dangerouslySetInnerHTML={{ __html: post.html }}></p>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: {slug: {eq: $slug}}) {
      html
      frontmatter {
        title
      }
    }
  }
`

export default BlogPost